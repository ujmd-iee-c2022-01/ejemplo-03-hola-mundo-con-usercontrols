﻿
namespace Hola_Mundo_con_UserControls
{
    partial class frmPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrincipal));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.userControl91 = new Hola_Mundo_con_UserControls.UserControl9();
            this.userControl81 = new Hola_Mundo_con_UserControls.UserControl8();
            this.userControl71 = new Hola_Mundo_con_UserControls.UserControl7();
            this.userControl61 = new Hola_Mundo_con_UserControls.UserControl6();
            this.userControl51 = new Hola_Mundo_con_UserControls.UserControl5();
            this.userControl41 = new Hola_Mundo_con_UserControls.UserControl4();
            this.userControl31 = new Hola_Mundo_con_UserControls.UserControl3();
            this.userControl21 = new Hola_Mundo_con_UserControls.UserControl2();
            this.userControl11 = new Hola_Mundo_con_UserControls.UserControl1();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.radioButton8 = new System.Windows.Forms.RadioButton();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Maroon;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.radioButton1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.radioButton2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.radioButton3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.radioButton4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.radioButton5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.radioButton6, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.radioButton7, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.radioButton8, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.radioButton9, 0, 9);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 11;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(773, 458);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Lavender;
            this.pictureBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox2.Image = global::Hola_Mundo_con_UserControls.Properties.Resources.Racoon_Mario;
            this.pictureBox2.Location = new System.Drawing.Point(5, 5);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(5, 5, 5, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(130, 132);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Lavender;
            this.panel1.Controls.Add(this.userControl91);
            this.panel1.Controls.Add(this.userControl81);
            this.panel1.Controls.Add(this.userControl71);
            this.panel1.Controls.Add(this.userControl61);
            this.panel1.Controls.Add(this.userControl51);
            this.panel1.Controls.Add(this.userControl41);
            this.panel1.Controls.Add(this.userControl31);
            this.panel1.Controls.Add(this.userControl21);
            this.panel1.Controls.Add(this.userControl11);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(140, 5);
            this.panel1.Margin = new System.Windows.Forms.Padding(0, 5, 5, 3);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 11);
            this.panel1.Size = new System.Drawing.Size(628, 450);
            this.panel1.TabIndex = 3;
            // 
            // userControl91
            // 
            this.userControl91.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("userControl91.BackgroundImage")));
            this.userControl91.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.userControl91.Location = new System.Drawing.Point(250, 193);
            this.userControl91.Name = "userControl91";
            this.userControl91.Size = new System.Drawing.Size(351, 248);
            this.userControl91.TabIndex = 10;
            this.userControl91.Visible = false;
            // 
            // userControl81
            // 
            this.userControl81.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("userControl81.BackgroundImage")));
            this.userControl81.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.userControl81.Location = new System.Drawing.Point(228, 173);
            this.userControl81.Name = "userControl81";
            this.userControl81.Size = new System.Drawing.Size(325, 234);
            this.userControl81.TabIndex = 9;
            this.userControl81.Visible = false;
            // 
            // userControl71
            // 
            this.userControl71.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("userControl71.BackgroundImage")));
            this.userControl71.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.userControl71.Location = new System.Drawing.Point(188, 138);
            this.userControl71.Name = "userControl71";
            this.userControl71.Size = new System.Drawing.Size(313, 226);
            this.userControl71.TabIndex = 8;
            this.userControl71.Visible = false;
            // 
            // userControl61
            // 
            this.userControl61.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("userControl61.BackgroundImage")));
            this.userControl61.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.userControl61.Location = new System.Drawing.Point(150, 122);
            this.userControl61.Name = "userControl61";
            this.userControl61.Size = new System.Drawing.Size(276, 210);
            this.userControl61.TabIndex = 7;
            this.userControl61.Visible = false;
            // 
            // userControl51
            // 
            this.userControl51.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("userControl51.BackgroundImage")));
            this.userControl51.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.userControl51.Location = new System.Drawing.Point(121, 87);
            this.userControl51.Name = "userControl51";
            this.userControl51.Size = new System.Drawing.Size(257, 220);
            this.userControl51.TabIndex = 6;
            this.userControl51.Visible = false;
            // 
            // userControl41
            // 
            this.userControl41.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("userControl41.BackgroundImage")));
            this.userControl41.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.userControl41.Location = new System.Drawing.Point(84, 66);
            this.userControl41.Name = "userControl41";
            this.userControl41.Size = new System.Drawing.Size(256, 192);
            this.userControl41.TabIndex = 5;
            this.userControl41.Visible = false;
            // 
            // userControl31
            // 
            this.userControl31.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("userControl31.BackgroundImage")));
            this.userControl31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.userControl31.Location = new System.Drawing.Point(58, 45);
            this.userControl31.Name = "userControl31";
            this.userControl31.Size = new System.Drawing.Size(256, 192);
            this.userControl31.TabIndex = 4;
            this.userControl31.Visible = false;
            // 
            // userControl21
            // 
            this.userControl21.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("userControl21.BackgroundImage")));
            this.userControl21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.userControl21.Location = new System.Drawing.Point(29, 20);
            this.userControl21.Name = "userControl21";
            this.userControl21.Size = new System.Drawing.Size(256, 192);
            this.userControl21.TabIndex = 3;
            this.userControl21.Visible = false;
            // 
            // userControl11
            // 
            this.userControl11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("userControl11.BackgroundImage")));
            this.userControl11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.userControl11.Location = new System.Drawing.Point(15, 0);
            this.userControl11.Name = "userControl11";
            this.userControl11.Size = new System.Drawing.Size(256, 192);
            this.userControl11.TabIndex = 2;
            this.userControl11.Visible = false;
            // 
            // radioButton1
            // 
            this.radioButton1.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton1.BackColor = System.Drawing.Color.DarkRed;
            this.radioButton1.Checked = true;
            this.radioButton1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton1.FlatAppearance.BorderSize = 2;
            this.radioButton1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.radioButton1.ForeColor = System.Drawing.Color.White;
            this.radioButton1.Location = new System.Drawing.Point(3, 143);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(134, 29);
            this.radioButton1.TabIndex = 4;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Hola Mundo 1";
            this.radioButton1.UseVisualStyleBackColor = false;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton2.BackColor = System.Drawing.Color.DarkRed;
            this.radioButton2.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton2.FlatAppearance.BorderSize = 2;
            this.radioButton2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.radioButton2.ForeColor = System.Drawing.Color.White;
            this.radioButton2.Location = new System.Drawing.Point(3, 178);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(134, 29);
            this.radioButton2.TabIndex = 4;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Hola Mundo 2";
            this.radioButton2.UseVisualStyleBackColor = false;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton3.BackColor = System.Drawing.Color.DarkRed;
            this.radioButton3.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton3.FlatAppearance.BorderSize = 2;
            this.radioButton3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.radioButton3.ForeColor = System.Drawing.Color.White;
            this.radioButton3.Location = new System.Drawing.Point(3, 213);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(134, 29);
            this.radioButton3.TabIndex = 4;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "Hola Mundo 3";
            this.radioButton3.UseVisualStyleBackColor = false;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton4
            // 
            this.radioButton4.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton4.BackColor = System.Drawing.Color.DarkRed;
            this.radioButton4.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton4.FlatAppearance.BorderSize = 2;
            this.radioButton4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.radioButton4.ForeColor = System.Drawing.Color.White;
            this.radioButton4.Location = new System.Drawing.Point(3, 248);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(134, 29);
            this.radioButton4.TabIndex = 4;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "Hola Mundo 4";
            this.radioButton4.UseVisualStyleBackColor = false;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton5
            // 
            this.radioButton5.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton5.BackColor = System.Drawing.Color.DarkRed;
            this.radioButton5.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton5.FlatAppearance.BorderSize = 2;
            this.radioButton5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.radioButton5.ForeColor = System.Drawing.Color.White;
            this.radioButton5.Location = new System.Drawing.Point(3, 283);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(134, 29);
            this.radioButton5.TabIndex = 4;
            this.radioButton5.TabStop = true;
            this.radioButton5.Text = "Hola Mundo 5";
            this.radioButton5.UseVisualStyleBackColor = false;
            this.radioButton5.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton6
            // 
            this.radioButton6.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton6.BackColor = System.Drawing.Color.DarkRed;
            this.radioButton6.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton6.FlatAppearance.BorderSize = 2;
            this.radioButton6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.radioButton6.ForeColor = System.Drawing.Color.White;
            this.radioButton6.Location = new System.Drawing.Point(3, 318);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(134, 29);
            this.radioButton6.TabIndex = 4;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "Hola Mundo 6";
            this.radioButton6.UseVisualStyleBackColor = false;
            this.radioButton6.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton7
            // 
            this.radioButton7.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton7.BackColor = System.Drawing.Color.DarkRed;
            this.radioButton7.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton7.FlatAppearance.BorderSize = 2;
            this.radioButton7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.radioButton7.ForeColor = System.Drawing.Color.White;
            this.radioButton7.Location = new System.Drawing.Point(3, 353);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(134, 29);
            this.radioButton7.TabIndex = 4;
            this.radioButton7.TabStop = true;
            this.radioButton7.Text = "Hola Mundo 7";
            this.radioButton7.UseVisualStyleBackColor = false;
            this.radioButton7.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton8
            // 
            this.radioButton8.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton8.BackColor = System.Drawing.Color.DarkRed;
            this.radioButton8.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton8.FlatAppearance.BorderSize = 2;
            this.radioButton8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.radioButton8.ForeColor = System.Drawing.Color.White;
            this.radioButton8.Location = new System.Drawing.Point(3, 388);
            this.radioButton8.Name = "radioButton8";
            this.radioButton8.Size = new System.Drawing.Size(134, 29);
            this.radioButton8.TabIndex = 4;
            this.radioButton8.TabStop = true;
            this.radioButton8.Text = "Hola Mundo 8";
            this.radioButton8.UseVisualStyleBackColor = false;
            this.radioButton8.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton9
            // 
            this.radioButton9.Appearance = System.Windows.Forms.Appearance.Button;
            this.radioButton9.BackColor = System.Drawing.Color.DarkRed;
            this.radioButton9.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.radioButton9.FlatAppearance.BorderSize = 2;
            this.radioButton9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.radioButton9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold);
            this.radioButton9.ForeColor = System.Drawing.Color.White;
            this.radioButton9.Location = new System.Drawing.Point(3, 423);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(134, 29);
            this.radioButton9.TabIndex = 4;
            this.radioButton9.TabStop = true;
            this.radioButton9.Text = "Adiós";
            this.radioButton9.UseVisualStyleBackColor = false;
            this.radioButton9.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // frmPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 458);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(736, 497);
            this.Name = "frmPrincipal";
            this.Text = "Hola Mundo";
            this.Load += new System.EventHandler(this.frmPrincipal_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel panel1;
        private UserControl4 userControl41;
        private UserControl3 userControl31;
        private UserControl2 userControl21;
        private UserControl1 userControl11;
        private UserControl8 userControl81;
        private UserControl7 userControl71;
        private UserControl6 userControl61;
        private UserControl5 userControl51;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.RadioButton radioButton8;
        private System.Windows.Forms.RadioButton radioButton9;
        private UserControl9 userControl91;
        private System.Windows.Forms.Timer timer1;
    }
}

