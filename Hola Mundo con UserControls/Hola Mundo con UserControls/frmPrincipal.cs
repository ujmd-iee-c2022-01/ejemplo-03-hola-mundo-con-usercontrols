﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hola_Mundo_con_UserControls
{
    public partial class frmPrincipal : Form
    {
        public frmPrincipal()
        {
            InitializeComponent();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            // Forma de hacer visible un UserControl mediante IF y la propiedad Checked
            if (radioButton1.Checked == true)
                userControl11.Visible = true;
            else
                userControl11.Visible = false;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            // Forma de hacer visible un UserControl mediante asignación directa
            // de la propiedad Checked
            userControl21.Visible = radioButton2.Checked;
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            userControl31.Visible = radioButton3.Checked;
            userControl41.Visible = radioButton4.Checked;
            userControl51.Visible = radioButton5.Checked;
            userControl61.Visible = radioButton6.Checked;
            userControl71.Visible = radioButton7.Checked;
            userControl81.Visible = radioButton8.Checked;
            userControl91.Visible = radioButton9.Checked;

            if (radioButton9.Checked)
            {
                //timer1.Enabled = true;
                timer1.Start();
            }
        }

        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            userControl11.Dock = DockStyle.Fill;
            userControl21.Dock = DockStyle.Fill;
            userControl31.Dock = DockStyle.Fill;
            userControl41.Dock = DockStyle.Fill;
            userControl51.Dock = DockStyle.Fill;
            userControl61.Dock = DockStyle.Fill;
            userControl71.Dock = DockStyle.Fill;
            userControl81.Dock = DockStyle.Fill;
            userControl91.Dock = DockStyle.Fill;
            radioButton1_CheckedChanged(sender,e);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.Close();
            this.Dispose();
        }
    }
}
